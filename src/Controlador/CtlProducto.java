/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import DAO.ProductoDAO;
import Modelo.Producto;
import javax.swing.table.TableModel;

/**
 *
 * @author sebas
 */
public class CtlProducto {

    public TableModel SolicitudListarProductos() {
        ProductoDAO productoDAO = new ProductoDAO();
        return productoDAO.listarProductos();    
    }

    public boolean registrarProducto(int idProducto, String descripcionProducto, double precioProducto, double costoProducto, String unidadMedidaProducto) {
        Producto producto = new Producto(idProducto, descripcionProducto, precioProducto, costoProducto, unidadMedidaProducto);
        ProductoDAO productoDAO = new ProductoDAO();
        return productoDAO.guardarProducto(producto);
    }

    public Producto SolicitudBuscarProducto(int idProducto) {
        ProductoDAO productoDAO = new ProductoDAO();
        return productoDAO.buscarProducto(idProducto);
    }

    public boolean SolicitudModificarProducto(int idProducto, String descripcionProducto, double precioProducto, double costoProducto, String unidadMedida) {
        Producto producto = new Producto(idProducto, descripcionProducto, precioProducto, costoProducto, unidadMedida);
        ProductoDAO productoDAO = new ProductoDAO();
        return productoDAO.modificarProducto(producto);   
    }
    
    public boolean validarComprasProducto(int idProducto) {
        ProductoDAO productoDAO = new ProductoDAO();
        return productoDAO.validarProductoFactura(idProducto);
    }

    public boolean solicitudEliminarProducto(int idProducto) {
        ProductoDAO productoDAO = new ProductoDAO();
        return productoDAO.eliminarProducto(idProducto);        
    }
}
