/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import DAO.PersonaDAO;
import Modelo.Persona;
import javax.swing.table.TableModel;

/**
 *
 * @author sebas
 */
public class CtlPersona {


    public boolean registrarPersona(String numDocPersona, String nombrePersona, String apellidoPersona, String tipoDocPersona) {
        Persona persona = new Persona(nombrePersona, apellidoPersona, tipoDocPersona, numDocPersona);
        PersonaDAO personaDAO = new PersonaDAO();
        return personaDAO.guardarPersona(persona);

    }

    public Persona SolicitudBuscarPersona(String numeroDocPersona) {
        PersonaDAO personaDAO = new PersonaDAO();
        return personaDAO.buscarPersona(numeroDocPersona);
    }

    public TableModel SolicitudListarPersonas() {
        PersonaDAO personaDAO = new PersonaDAO();
        return personaDAO.listarPersonas();
    }

    public boolean SolicitudModificarPersona(int idPersona, String nombrePersona, String ApellidoPersona, String tipoDocPersona, String documentoPersona) {
        Persona persona = new Persona(idPersona, nombrePersona, ApellidoPersona, tipoDocPersona, documentoPersona);
        PersonaDAO personaDAO = new PersonaDAO();
        return personaDAO.modificarPersona(persona);    
    }

    public boolean solicitudEliminarPersona(String documentoPersona) {
        PersonaDAO personaDAO = new PersonaDAO();
        return personaDAO.eliminarPersona(documentoPersona);    
    }
    
    public boolean validarComprasPersona(int idPersona) {
        PersonaDAO personaDAO = new PersonaDAO();
        return personaDAO.validarPersonaFactura(idPersona);
    }
}
