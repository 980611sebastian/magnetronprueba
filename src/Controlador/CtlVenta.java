/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import DAO.VentaDAO;
import Modelo.VentaDetalle;
import Modelo.VentaEncabezado;

/**
 *
 * @author sebas
 */
public class CtlVenta {

    public boolean registrarVenta(int numeroFactura, String fechaFactura, int idPersona, double totalFactura) {
        VentaEncabezado ventaEncabezado = new VentaEncabezado(numeroFactura, fechaFactura, idPersona, totalFactura);
        VentaDAO ventaDAO = new VentaDAO();
        return ventaDAO.guardarVenta(ventaEncabezado);
    }

    public int obtenerIdVenta() {
        VentaDAO ventaDAO = new VentaDAO();
        return ventaDAO.ObtenerIdVentaEnc();
    }

    public int obtenerNroFactura() {
        VentaDAO ventaDAO = new VentaDAO();
        return ventaDAO.obtenerNroFactura();
    }

    public boolean registrarDetalleVenta(String linea, int cantidad, int idProducto, int idVenta) {
        VentaDetalle ventaDetalle = new VentaDetalle(linea, cantidad, idProducto, idVenta);
        VentaDAO ventaDAO = new VentaDAO();
        return ventaDAO.guardarDetalleVenta(ventaDetalle);
    }

}
