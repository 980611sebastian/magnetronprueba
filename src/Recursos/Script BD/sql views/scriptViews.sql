-- Vista 1: Total facturado por persona
CREATE OR REPLACE VIEW TotalFacturadoPorPersona AS
SELECT 
    p.Per_ID,
    p.Per_Nombre,
    p.Per_Apellido,
    COALESCE(SUM(f.FEnc_Total), 0) AS TotalFacturado
FROM 
    Persona p
LEFT JOIN 
    Fact_Encabezado f ON p.Per_ID = f.zPer_ID
GROUP BY 
    p.Per_ID, p.Per_Nombre, p.Per_Apellido;

-- Vista 2: Persona que compró el producto más caro
CREATE OR REPLACE VIEW PersonaProductoMasCaro AS
SELECT 
    p.Per_ID,
    p.Per_Nombre,
    p.Per_Apellido,
    f.FEnc_Total,
    pr.Prod_Descripcion,
    pr.Prod_Precio
FROM 
    Persona p
JOIN 
    Fact_Encabezado f ON p.Per_ID = f.zPer_ID
JOIN 
    Fact_Detalle fd ON f.FEnc_ID = fd.zFEnc_ID
JOIN 
    Producto pr ON fd.zProd_ID = pr.Prod_ID
ORDER BY 
    pr.Prod_Precio DESC
LIMIT 1;

-- Vista 3: Productos por cantidad facturada
CREATE OR REPLACE VIEW ProductosPorCantidadFacturada AS
SELECT 
    pr.Prod_ID,
    pr.Prod_Descripcion,
    SUM(fd.FDet_Cantidad) AS CantidadFacturada
FROM 
    Producto pr
JOIN 
    Fact_Detalle fd ON pr.Prod_ID = fd.zProd_ID
GROUP BY 
    pr.Prod_ID, pr.Prod_Descripcion
ORDER BY 
    CantidadFacturada DESC;

-- Vista 4: Productos por utilidad generada por facturación
CREATE OR REPLACE VIEW ProductosPorUtilidad AS
SELECT 
    pr.Prod_ID,
    pr.Prod_Descripcion,
    SUM((fd.FDet_Cantidad * pr.Prod_Precio) - (fd.FDet_Cantidad * pr.Prod_Costo)) AS Utilidad
FROM 
    Producto pr
JOIN 
    Fact_Detalle fd ON pr.Prod_ID = fd.zProd_ID
GROUP BY 
    pr.Prod_ID, pr.Prod_Descripcion
ORDER BY 
    Utilidad DESC;

-- Vista 5: Productos por margen de ganancia
CREATE OR REPLACE VIEW ProductosPorMargenDeGanancia AS
SELECT 
    pr.Prod_ID,
    pr.Prod_Descripcion,
    (SUM(fd.FDet_Cantidad * pr.Prod_Precio) - SUM(fd.FDet_Cantidad * pr.Prod_Costo)) / SUM(fd.FDet_Cantidad * pr.Prod_Costo) AS MargenDeGanancia
FROM 
    Producto pr
JOIN 
    Fact_Detalle fd ON pr.Prod_ID = fd.zProd_ID
GROUP BY 
    pr.Prod_ID, pr.Prod_Descripcion
ORDER BY 
    MargenDeGanancia DESC;