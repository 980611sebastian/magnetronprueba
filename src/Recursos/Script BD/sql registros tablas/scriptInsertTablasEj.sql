
-- insert fact_detalle
INSERT INTO `fact_detalle` VALUES (1,'venta1',1,1,1),(2,'venta1',1,2,1),(3,'ventaArmenia',2,3,2),(4,'ventaArmenia',1,1,2),(5,'ventaPereria',1,1,3),(6,'ventaPereria',5,3,3),(7,'ventaMedellin',2,1,4),(8,'ventaMedellin',1,3,4),(9,'ventaArmenia',5,3,5),(10,'ventaArmenia',2,2,5);


-- insert fact_encabezado
INSERT INTO `fact_encabezado` VALUES (1,1,'2024-06-23',1,2200000),(2,2,'2024-06-24',5,2040000),(3,3,'2024-06-24',4,2100000),(4,4,'2024-06-24',4,4020000),(5,5,'2024-06-24',1,102000);


-- insert persona
INSERT INTO `persona` VALUES (1,'sebastian','villegas','Cedula','1096040425'),(4,'camilo','perez','Pasaporte','123'),(5,'juan','agudelo','Cedula','1111');


-- insert producto
INSERT INTO `producto` VALUES (1,'llanta carro mazda',2000000,1200000,'Metro (M)'),(2,'prueba',1000,100,'Metro (M)'),(3,'neumatico cicla',20000,5000,'Metro (M)');
