-- BASE DE DATOS
CREATE DATABASE  IF NOT EXISTS `pruebamagnetron` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

-- SELECCIONAR BASE DE DATOS
USE `pruebamagnetron`;
-- TABLAS

DROP TABLE IF EXISTS `fact_detalle`;

CREATE TABLE `fact_detalle` (
  `FDet_ID` int NOT NULL AUTO_INCREMENT,
  `FDet_Linea` varchar(50) NOT NULL,
  `FDet_Cantidad` int NOT NULL,
  `zProd_ID` int NOT NULL,
  `zFEnc_ID` int NOT NULL,
  PRIMARY KEY (`FDet_ID`),
  KEY `Prod_ID` (`zProd_ID`),
  KEY `FEnc_ID` (`zFEnc_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `fact_encabezado`;

CREATE TABLE `fact_encabezado` (
  `FEnc_ID` int NOT NULL AUTO_INCREMENT,
  `FEnc_Numero` int NOT NULL,
  `FEnc_Fecha` date NOT NULL,
  `zPer_ID` int DEFAULT NULL,
  `FEnc_Total` double NOT NULL,
  PRIMARY KEY (`FEnc_ID`),
  KEY `Per_ID` (`zPer_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `persona`;

CREATE TABLE `persona` (
  `Per_ID` int NOT NULL AUTO_INCREMENT,
  `Per_Nombre` varchar(50) NOT NULL,
  `Per_Apellido` varchar(50) NOT NULL,
  `Per_TipoDocumento` varchar(50) NOT NULL,
  `Per_Documento` varchar(50) NOT NULL,
  PRIMARY KEY (`Per_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `producto`;

CREATE TABLE `producto` (
  `Prod_ID` int NOT NULL,
  `Prod_Descripcion` varchar(100) DEFAULT NULL,
  `Prod_Precio` double NOT NULL,
  `Prod_Costo` double NOT NULL,
  `Prod_UM` varchar(50) NOT NULL,
  PRIMARY KEY (`Prod_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
