/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author sebas
 */
public class Producto {
    
    int idProducto;
    String descripcionProducto;
    double precioProducto;
    double costoProducto;
    String unidadMedidaProducto;

    public Producto(String descripcionProducto, double precioProducto, double costoProducto, String unidadMedidaProducto) {
        this.descripcionProducto = descripcionProducto;
        this.precioProducto = precioProducto;
        this.costoProducto = costoProducto;
        this.unidadMedidaProducto = unidadMedidaProducto;
    }

    public Producto(int idProducto, String descripcionProducto, double precioProducto, double costoProducto, String unidadMedidaProducto) {
        this.idProducto = idProducto;
        this.descripcionProducto = descripcionProducto;
        this.precioProducto = precioProducto;
        this.costoProducto = costoProducto;
        this.unidadMedidaProducto = unidadMedidaProducto;
    }

    public Producto() {
    }
    

    public String getDescripcionProducto() {
        return descripcionProducto;
    }

    public void setDescripcionProducto(String descripcionProducto) {
        this.descripcionProducto = descripcionProducto;
    }

    public double getPrecioProducto() {
        return precioProducto;
    }

    public void setPrecioProducto(double precioProducto) {
        this.precioProducto = precioProducto;
    }

    public double getCostoProducto() {
        return costoProducto;
    }

    public void setCostoProducto(double costoProducto) {
        this.costoProducto = costoProducto;
    }

    public String getUnidadMedidaProducto() {
        return unidadMedidaProducto;
    }

    public void setUnidadMedidaProducto(String unidadMedidaProducto) {
        this.unidadMedidaProducto = unidadMedidaProducto;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }
    
    
}
