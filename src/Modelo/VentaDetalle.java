/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author sebas
 */
public class VentaDetalle {
    
    int idDetalleFactura;
    String lineaFacturaDetalle;
    int cantidadFacturaDetalle;
    int idProducto;
    int idEncFactura;

    public VentaDetalle(String lineaFacturaDetalle, int cantidadFacturaDetalle, int idProducto, int idEncFactura) {
        this.lineaFacturaDetalle = lineaFacturaDetalle;
        this.cantidadFacturaDetalle = cantidadFacturaDetalle;
        this.idProducto = idProducto;
        this.idEncFactura = idEncFactura;
    }

    public VentaDetalle(int idDetalleFactura, String lineaFacturaDetalle, int cantidadFacturaDetalle, int idProducto, int idEncFactura) {
        this.idDetalleFactura = idDetalleFactura;
        this.lineaFacturaDetalle = lineaFacturaDetalle;
        this.cantidadFacturaDetalle = cantidadFacturaDetalle;
        this.idProducto = idProducto;
        this.idEncFactura = idEncFactura;
    }
    
    

    public String getLineaFacturaDetalle() {
        return lineaFacturaDetalle;
    }

    public void setLineaFacturaDetalle(String lineaFacturaDetalle) {
        this.lineaFacturaDetalle = lineaFacturaDetalle;
    }

    public int getCantidadFacturaDetalle() {
        return cantidadFacturaDetalle;
    }

    public void setCantidadFacturaDetalle(int cantidadFacturaDetalle) {
        this.cantidadFacturaDetalle = cantidadFacturaDetalle;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getIdEncFactura() {
        return idEncFactura;
    }

    public void setIdEncFactura(int idEncFactura) {
        this.idEncFactura = idEncFactura;
    }

    public int getIdDetalleFactura() {
        return idDetalleFactura;
    }

    public void setIdDetalleFactura(int idDetalleFactura) {
        this.idDetalleFactura = idDetalleFactura;
    }
    
    
}
