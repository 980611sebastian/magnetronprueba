/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author sebas
 */
public class Persona {
    int idPersona;
    String nombrePersona;
    String apellidoPersona;
    String tipoDocumentoPersona;
    String documentoPersona;

    public Persona(String nombrePersona, String apellidoPersona, String tipoDocumentoPersona, String documentoPersona) {
        this.nombrePersona = nombrePersona;
        this.apellidoPersona = apellidoPersona;
        this.tipoDocumentoPersona = tipoDocumentoPersona;
        this.documentoPersona = documentoPersona;
    }

    public Persona(int idPersona, String nombrePersona, String apellidoPersona, String tipoDocumentoPersona, String documentoPersona) {
        this.idPersona = idPersona;
        this.nombrePersona = nombrePersona;
        this.apellidoPersona = apellidoPersona;
        this.tipoDocumentoPersona = tipoDocumentoPersona;
        this.documentoPersona = documentoPersona;
    }
    

    public Persona() {
    }

    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

    public String getApellidoPersona() {
        return apellidoPersona;
    }

    public void setApellidoPersona(String apellidoPersona) {
        this.apellidoPersona = apellidoPersona;
    }

    public String getTipoDocumentoPersona() {
        return tipoDocumentoPersona;
    }

    public void setTipoDocumentoPersona(String tipoDocumentoPersona) {
        this.tipoDocumentoPersona = tipoDocumentoPersona;
    }

    public String getDocumentoPersona() {
        return documentoPersona;
    }

    public void setDocumentoPersona(String documentoPersona) {
        this.documentoPersona = documentoPersona;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }
    
    

}
