/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author sebas
 */
public class VentaEncabezado {
    
    int idFacturaEnc;
    int numeroFacEnc;
    String fechaFacEnc;
    int idPersona;
    double total;

    public VentaEncabezado(int numeroFacEnc, String fechaFacEnc, int idPersona, double total) {
        this.numeroFacEnc = numeroFacEnc;
        this.fechaFacEnc = fechaFacEnc;
        this.idPersona = idPersona;
        this.total = total;
    }



    public VentaEncabezado(int idFacturaEnc, int numeroFacEnc, String fechaFacEnc, int idPersona, double total) {
        this.idFacturaEnc = idFacturaEnc;
        this.numeroFacEnc = numeroFacEnc;
        this.fechaFacEnc = fechaFacEnc;
        this.idPersona = idPersona;
        this.total = total;
    }

    public VentaEncabezado() {
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
    public int getNumeroFacEnc() {
        return numeroFacEnc;
    }

    public void setNumeroFacEnc(int numeroFacEnc) {
        this.numeroFacEnc = numeroFacEnc;
    }

    public String getFechaFacEnc() {
        return fechaFacEnc;
    }

    public void setFechaFacEnc(String fechaFacEnc) {
        this.fechaFacEnc = fechaFacEnc;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public int getIdFacturaEnc() {
        return idFacturaEnc;
    }

    public void setIdFacturaEnc(int idFacturaEnc) {
        this.idFacturaEnc = idFacturaEnc;
    }
    
    
}
