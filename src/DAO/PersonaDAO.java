/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import Modelo.Persona;
import Modelo.conexion;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author sebas
 */
public class PersonaDAO extends conexion {

    Persona persona = new Persona();

    public boolean guardarPersona(Persona persona) {
        String consulta = "insert into persona (Per_ID,Per_Nombre,Per_Apellido,Per_TipoDocumento,Per_Documento) values(null,'" + persona.getNombrePersona() + "','" + persona.getApellidoPersona() + "','" + persona.getTipoDocumentoPersona() + "','" + persona.getDocumentoPersona() + "')";
        System.out.println(consulta);
        return super.ejecutar(consulta);
    }

    public Persona buscarPersona(String numeroDocPersona) {
        String consulta = "SELECT Per_ID,Per_Nombre,Per_Apellido,Per_TipoDocumento,Per_Documento FROM persona where Per_Documento='" + numeroDocPersona + "'";
        System.out.println(consulta);

        super.ejecutarRetorno(consulta);
        try {
            if (resultadoDB.next()) {
                persona.setIdPersona(resultadoDB.getInt("Per_ID"));
                persona.setNombrePersona(resultadoDB.getString("Per_Nombre"));
                persona.setApellidoPersona(resultadoDB.getString("Per_Apellido"));
                persona.setTipoDocumentoPersona(resultadoDB.getString("Per_TipoDocumento"));
                persona.setDocumentoPersona(resultadoDB.getString("Per_Documento"));
            }
        } catch (SQLException ex) {
            System.out.println("error al listar campos en el form para permitir modificar");
        }
        return persona;
    }

    public TableModel listarPersonas() {
        DefaultTableModel modelTabla;
        String nombreColumnas[] = {"TIPO DOC", "NUMERO DOC", "NOMBRE", "APELLIDO"};
        modelTabla = new DefaultTableModel(new Object[][]{}, nombreColumnas);

        String consulta = "SELECT * FROM persona";
        super.ejecutarRetorno(consulta);

        try {

            while (resultadoDB.next()) {
                modelTabla.addRow(new Object[]{
                    resultadoDB.getString("Per_TipoDocumento"),
                    resultadoDB.getString("Per_Documento"),
                    resultadoDB.getString("Per_Nombre"),
                    resultadoDB.getString("Per_Apellido")});
            }
            //modelTabla.removeRow(0);
        } catch (SQLException ex) {
            System.out.println("error al listar en la tabla de personas");
        }
        return modelTabla;
    }

    public boolean modificarPersona(Persona persona) {
        String consulta = "update persona set Per_ID='" + persona.getIdPersona() + "',Per_Nombre='" + persona.getNombrePersona() + "',Per_Apellido='" + persona.getApellidoPersona() + "',Per_TipoDocumento='" + persona.getTipoDocumentoPersona() + "',Per_Documento='" + persona.getDocumentoPersona() + "'" + " where Per_ID='" + persona.getIdPersona() + "'";
        System.out.println(consulta);
        return super.ejecutar(consulta);
    }

    public boolean eliminarPersona(String documentoPersona) {
        String consulta = "delete from persona where Per_Documento=" + "'" + documentoPersona + "'";
        System.out.println(consulta);
        return super.ejecutar(consulta);
    }

    public boolean validarPersonaFactura(int idPersona) {
        boolean resul = false;
        int variable = 0;
        String consulta = "select zPer_ID from fact_encabezado where zPer_ID=" + "" + idPersona + "";
        super.ejecutarRetorno(consulta);
        try {
            while (resultadoDB.next()) {
                variable = resultadoDB.getInt(1);
                resul = true;
            }
        } catch (SQLException ex) {
            System.out.println("error al encontrar id del encabezado de la venta");
        }
        return resul;
    }
}
