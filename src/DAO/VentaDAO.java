/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import Modelo.VentaDetalle;
import Modelo.VentaEncabezado;
import Modelo.conexion;
import java.sql.SQLException;

/**
 *
 * @author sebas
 */
public class VentaDAO extends conexion {


    public boolean guardarVenta(VentaEncabezado ventaEncabezado) {
        String consulta = "insert into fact_encabezado (FEnc_ID,FEnc_Numero,FEnc_Fecha,zPer_ID,FEnc_Total) values(null,'" + ventaEncabezado.getNumeroFacEnc() + "','" + ventaEncabezado.getFechaFacEnc() + "','" + ventaEncabezado.getIdPersona() + "','" + ventaEncabezado.getTotal() + "')";
        System.out.println(consulta);
        return super.ejecutar(consulta);
    }

    public boolean guardarDetalleVenta(VentaDetalle ventaDetalle) {
        String consulta = "insert into fact_detalle (FDet_ID,FDet_Linea,FDet_Cantidad,zProd_ID,zFEnc_ID) values(null,'" + ventaDetalle.getLineaFacturaDetalle() + "','" + ventaDetalle.getCantidadFacturaDetalle() + "','" + ventaDetalle.getIdProducto() + "','" + ventaDetalle.getIdEncFactura() + "')";
        System.out.println(consulta);
        return super.ejecutar(consulta);
    }

    public int ObtenerIdVentaEnc() {
        int idVentaEnc = 0;
        String consulta = "SELECT max(FEnc_ID) FROM fact_encabezado";
        super.ejecutarRetorno(consulta);
        try {
            while (resultadoDB.next()) {
                idVentaEnc = resultadoDB.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("error al encontrar id del encabezado de la venta");
        }
        return idVentaEnc;
    }

    public int obtenerNroFactura() {
        int numFactura = 0;
        String consulta = "select max(FEnc_Numero) from fact_encabezado";
        super.ejecutarRetorno(consulta);
        try {
            while (resultadoDB.next()) {
                numFactura = resultadoDB.getInt(1);
            }
        } catch (SQLException ex) {
        }
        return numFactura;
    }

}
