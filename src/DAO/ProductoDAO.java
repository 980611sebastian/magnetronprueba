/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import Modelo.Producto;
import Modelo.conexion;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author sebas
 */
public class ProductoDAO extends conexion{
    
    Producto producto = new Producto();

    public TableModel listarProductos() {
        DefaultTableModel modelTabla;
        String nombreColumnas[] = {"ID", "DESCRIPCION", "PRECIO", "COSTO", "UND MEDIDA"};
        modelTabla = new DefaultTableModel(new Object[][]{}, nombreColumnas);

        String consulta = "SELECT * FROM producto";
        super.ejecutarRetorno(consulta);

        try {

            while (resultadoDB.next()) {
                modelTabla.addRow(new Object[]{
                    resultadoDB.getString("Prod_ID"),
                    resultadoDB.getString("Prod_Descripcion"),
                    resultadoDB.getString("Prod_Precio"),
                    resultadoDB.getString("Prod_Costo"),
                    resultadoDB.getString("Prod_UM")});
            }
           //modelTabla.removeRow(0);
        } catch (SQLException ex) {
            System.out.println("error al listar en la tabla de productos");
        }
        return modelTabla;    
    }

    public boolean guardarProducto(Producto producto) {
        String consulta = "insert into producto (Prod_ID,Prod_Descripcion,Prod_Precio,Prod_Costo,Prod_UM) values('"+producto.getIdProducto()+ "','" + producto.getDescripcionProducto()+ "','" + producto.getPrecioProducto()+ "','" + producto.getCostoProducto()+ "','" + producto.getUnidadMedidaProducto()+ "')";
        System.out.println(consulta);
        return super.ejecutar(consulta); 
    }

    public Producto buscarProducto(int idProducto) {
        String consulta = "SELECT Prod_ID,Prod_Descripcion,Prod_Precio,Prod_Costo,Prod_UM FROM producto where Prod_ID='" + idProducto + "'";
        System.out.println(consulta);

        super.ejecutarRetorno(consulta);
        try {
            if (resultadoDB.next()) {
                producto.setIdProducto(resultadoDB.getInt("Prod_ID"));
                producto.setDescripcionProducto(resultadoDB.getString("Prod_Descripcion"));
                producto.setPrecioProducto(resultadoDB.getDouble("Prod_Precio"));
                producto.setCostoProducto(resultadoDB.getDouble("Prod_Costo"));
                producto.setUnidadMedidaProducto(resultadoDB.getString("Prod_UM"));
            }
        } catch (SQLException ex) {
            System.out.println("error al listar campos en el form para permitir modificar");
        }
        return producto;   
    }

    public boolean modificarProducto(Producto producto) {
        String consulta = "update producto set Prod_ID='" + producto.getIdProducto()+ "',Prod_Descripcion='" + producto.getDescripcionProducto()+ "',Prod_Precio='" + producto.getPrecioProducto() + "',Prod_Costo='" + producto.getCostoProducto() + "',Prod_UM='" + producto.getUnidadMedidaProducto()+ "'" + " where Prod_ID='" + producto.getIdProducto()+ "'";
        System.out.println(consulta);
        return super.ejecutar(consulta);   
    }


    public boolean validarProductoFactura(int idProducto) {
        boolean resul = false;
        int variable = 0;
        String consulta = "select zProd_ID from fact_detalle where zProd_ID=" + "" + idProducto + "";
        super.ejecutarRetorno(consulta);
        try {
            while (resultadoDB.next()) {
                variable = resultadoDB.getInt(1);
                resul = true;
            }
        } catch (SQLException ex) {
            System.out.println("error al encontrar id del encabezado de la venta");
        }
        return resul;
    }

    public boolean eliminarProducto(int idProducto) {
        String consulta = "delete from producto where Prod_ID=" + "'" + idProducto + "'";
        System.out.println(consulta);
        return super.ejecutar(consulta);   
    }
   
}
