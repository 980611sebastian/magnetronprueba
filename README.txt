# Magnetrón

## Descripción

Magnetron es un sistema de gestión diseñado para facilitar la administración de productos, personas y ventas. Este proyecto incluye funcionalidades de facturación y gestión de inventarios, utilizando una interfaz gráfica de usuario y conexión a una base de datos MySQL.

## Requisitos

- Java JDK 8 o superior
- Apache Ant (para construir el proyecto) - se utilizo IDE NETBEANS
- MySQL (para la base de datos), se utilizo como motor de base de datos MySQLWorkbench
- Las siguientes bibliotecas adicionales:
    recuerden adicionar las librerías .jar antes de ejecutar el proyecto - ruta de los archivos (/magnetrón)
  - jcalendar-1.3.3.jar
  - mysql-connector-j-8.4.0.jar

## Instalación

1. Configurar conexión base de datos:

	al momento de crear la conexión a las bases de datos dejar los siguientes datos:
	
	Hostname: localhost o 127.0.0.1
        Port: 3306
        Username: root
	Password: 1234
	

2. Archivos (seleccionar solo una opción):

   2.1) Clonar el repositorio:
        bash
        git clone https://gitlab.com/980611sebastian/magnetronprueba.git
        cd magnetrón

   2.2) descargar el archivo .rar enviado al correo juandvelasquez@magnetron.com.co (drive), se encuentra todo lo necesario para el funcionamiento del
        proyecto
    

3. Configurar la base de datos MySQL: si no 

    - Crear la base de datos y las tablas ejecutando los scripts en src/Recursos/Script BD/sql creacion BD y tablas/scriptBDTablas.sql.
    - Crear vistas necesarias ejecutando los scripts en src/Recursos/Script BD/sql views/scriptViews.sql. (solucion punto 3)
    - (OPCIONAL) Insertar datos de ejemplo ejecutando los scripts en src/Recursos/Script BD/sql registros tablas/scriptInsertTablasEj.sql.

4. Proyecto:

	4.1) Si en el punto 2 se selecciono la opción 2.2 - Compilar el proyecto utilizando Netbeans: 
        abrir el proyecto en el IDE de NetBeans, se puede descargar el archivo .rar que se compartio en el correo de juandvelasquez@magnetron.com.co (drive)
	hay encontrara todo lo necesario para su ejecución y validación del proyecto

        4.2) Ejecutar el proyecto atreves del archivo generado .jar:

        en los archivos descargados en la ruta \magnetron\dist ejecutar el archivo magnetrón.jar el cual ya esta compilado, recuerde tener instalado sql
        y la base de datos creada para su correcto funcionamiento       

## Uso

El sistema proporciona varias interfaces gráficas para la gestión de personas, productos y ventas. Algunas de las principales interfaces incluyen:

- *Vista Login*: src/Vista/VistaLogin.java
	Con esta vista inicia la aplicación. Para la vista login se solicita datos de inicio de sesión los cuales por defecto son:
	username: admin
	password: 1234

- *Vista Principal del Menú*: src/Vista/VistaPrincipalMenu.java
- *Vista Administración de Personas y Productos*: src/Vista/VistaAdminPerPro.java
- *Vista de Facturación*: src/Vista/VistaFactura.java

## Ejemplos

A continuación se presentan algunos ejemplos de uso del sistema:

1. *Inicio de Sesión*:
    - Ejecute la aplicación y verá la pantalla de inicio de sesión.
    - Ingrese las credenciales de usuario para acceder al sistema.
	por defecto
	username: admin
	password: 1234

2. *Gestión de Productos*:
    - Navegue a la sección de gestión de productos para agregar, buscar, editar o eliminar productos del inventario.

3. *Gestión de Personas*:
    - Navegue a la sección de gestión de personas para agregar, buscar, editar o eliminar personas del sistema.

4. *Facturación*:
    - Utilice la vista de facturación para crear nuevas facturas, gestionar ventas.


## Dudas proyecto (configuración, ejecución, desarrollo)

Comunicarse a través del correo villegassebastian415@gmail.com


## Motivo herramientas seleccionadas:
   
Se desarrollo el proyecto con las tecnologías de JAVA ant (apache) - escritorio y persistencia en MySQL por el conocimiento en el lenguaje de programación,
además que en la prueba indicaban que preferiblemente realizarlo en JAVA


## Licencia

Este proyecto está licenciado bajo la Licencia MIT.




